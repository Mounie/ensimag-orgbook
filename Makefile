LU_MASTER= main
PDFLATEX_OPTIONS= -shell-escape

include LaTeX.mk


all: main.pdf main.html


main.tex: main.org org2tex.el
	emacs --visit=$< --batch -l org2tex  --kill

main.html: main.org org2html.el
	emacs --visit=$< --batch -l org2html --kill
