(org-mode)
(require 'ox-html)
(setq org-export-exclude-tags '("noexport"))
(org-add-link-type "exercice" nil
		   '(lambda (path desc frmt)
		      (cond ((eq frmt 'html) (format "<div class=\"exercice\"><h4>%s</h4>%s</div>" path desc))
			    ((eq frmt 'latex) (format "\\exercice{%s}{%s}" path desc))(t desc))))
(org-html-export-to-html)
