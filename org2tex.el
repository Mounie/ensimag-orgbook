(org-mode)
(setq org-latex-listings 'minted)
(require 'ox-latex)
;(message org-export-exclude-tags)
(setq org-export-exclude-tags '("html" "noexport"))
;(message org-export-exclude-tags)
(add-to-list 'org-latex-classes
	     '("ensimag-book" "\\documentclass{book}"
	       ("\\chapter{%s}" . "\\chapter*{%s}")
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
(org-add-link-type "exercice" nil
		   '(lambda (path desc frmt)
		      (cond ((eq frmt 'html) (format "<div class=\"exercice\"><h4>%s</h4>%s</div>" path desc))
			    ((eq frmt 'latex) (format "\\exercice{%s}{%s}" path desc))(t desc))))
(org-latex-export-to-latex)
