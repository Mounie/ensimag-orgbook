Org template and compilation chain to produce html/latex/pdf/epub
document with various tweaks for the equivalent of a small, medium
book

Format targets
==============

  * html document:
	* org info html export
	*  css for left menu like rust-book
  * pdf: 
    * org latex export
	* pygment for code: variable set in the org-mode, compilation use
      latex-make
  * epub (TODO):
    * use html export and convert with pandoc

Ideas and discussions
=====================
	In the Ideas.org, in French
